# A C++ library for a Vicon System

## Contributors

* Emil Fresk

---

## License

Licensed under the LGPL-v3 license, see LICENSE file for details.

---

Port of the ViconStream class form C#.
* Simple to use, see example file.
